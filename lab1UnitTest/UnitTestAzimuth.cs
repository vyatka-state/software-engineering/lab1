﻿using System;
using Lab1ClassLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace lab1UnitTest
{
    [TestClass]
    public class UnitTestAzimuth
    {
        [TestMethod]
        public void TestReturnValue()
        {
            //arrange
            var startPoint = new Point(1, 1);
            var endPoint = new Point(2, 20);
            
            //act
            var azimuth = SphericalUtils.GetAzimuth(startPoint, endPoint);
            
            //assert
            Assert.IsNotNull(azimuth);
        }

        [TestMethod]
        [DataRow(2, 2, 20, 20, 43.07, Azimuth.CONCRETE)] // проверка корректности расчёта азимута
        [DataRow(-180, -30, 0, 30, -2, Azimuth.ANY)] // точки на одной прямой, проходящей через центр Земли
        [DataRow(1, 1, 1, 1, -1, Azimuth.NONE)] // точки совпадают
        [DataRow(15, 90, 15, 15, 180, Azimuth.CONCRETE)] // одна из точек на северном полюсе
        [DataRow(15, 15, 15, 90, 180, Azimuth.CONCRETE)] // одна из точек на северном полюсе
        [DataRow(15, 90, 18, 90, -1, Azimuth.NONE)] // обе точки на северном полюсе
        [DataRow(15, -90, 18, -90, -1, Azimuth.NONE)] // обе точки на южном полюсе
        [DataRow(15, 90, 15, 90, -1, Azimuth.NONE)] // обе точки на северном полюсе и одинаковые
        [DataRow(55, 90, -5, -90, -2, Azimuth.ANY)] // точки на противоположных полюсах
        [DataRow(-5, -90, 55, 90, -2, Azimuth.ANY)] // точки на противоположных полюсах
        [DataRow(15, -90, 15, 15, 180, Azimuth.CONCRETE)] // одна точка на южном полюсе
        [DataRow(15, 15, 15, -90, 180, Azimuth.CONCRETE)] // одна точка на южном полюсе
        [DataRow(50, 30, 50, 50, 180, Azimuth.CONCRETE)] // идём на юг по одному меридиану
        [DataRow(50, 50, 50, -30, 0, Azimuth.CONCRETE)] // идём на север по одному меридиану
        [DataRow(90, 0, -45, 0, 270, Azimuth.CONCRETE)] // обе точки на экваторе ближе к 270
        [DataRow(40, 0, 50, 0, 90, Azimuth.CONCRETE)] // обе точки на экваторе ближе к 90
        public void TestMethodGetAzimuth(double point1Longitude, double point1Latitude, 
            double point2Longitude, double point2Latitude, 
            double expectedAzimuth, Azimuth expectedAzimuthType)
        {
            //arrange
            var startPoint = new Point(point1Longitude, point1Latitude);
            var endPoint = new Point(point2Longitude, point2Latitude);
            
            //act
            var azimuth = SphericalUtils.GetAzimuth(startPoint, endPoint);

            //assert
            Assert.AreEqual(expectedAzimuth, Math.Round(azimuth.Item1, 2));
            Assert.AreEqual(expectedAzimuthType, azimuth.Item2);
        }

        [TestMethod]
        [DataRow(2, 2, 20, 20, 2798.1)] // проверка корректности вычисления расстояния
        [DataRow(175, 30, -175, 30, 962.7)] // точки слева и справа от 180 меридиана
        public void TestDistanceCalculation(double point1Longitude, double point1Latitude, 
            double point2Longitude, double point2Latitude, double expectedDistance)
        {
            //arrange
            var startPoint = new Point(point1Longitude, point1Latitude);
            var endPoint = new Point(point2Longitude, point2Latitude);
            
            //act
            var distance = Math.Round(SphericalUtils.GetDistance(startPoint, endPoint), 1);
            
            //assert
            Assert.AreEqual(expectedDistance, distance);
        }
    }
}