﻿using System;
using Lab1ClassLibrary;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace lab1UnitTest
{
    [TestClass]
    public class UnitTestPoint
    {
        [TestMethod]
        public void TestMethodConstructor()
        {
            Point point = new Point(1.5, 1.02);
            Assert.IsNotNull(point);
        }

        [TestMethod, ExpectedException(typeof(ArgumentOutOfRangeException))]
        public void TestMethodOutOfRange()
        {
            new Point(200, 200);
        }
    }
}
