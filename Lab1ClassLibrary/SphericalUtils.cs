﻿using System;

namespace Lab1ClassLibrary
{
    public static class SphericalUtils
    {
        private static int EARTH_RADIUS = 6371;
        
        public static Tuple<double, Azimuth> GetAzimuth(Point startPoint, Point endPoint)
        {
            if (Math.Abs(endPoint.Longitude - startPoint.Longitude) == 180 && startPoint.Latitude + endPoint.Latitude == 0
                || Math.Abs(endPoint.Latitude - startPoint.Latitude) == 180)
            {
                return new Tuple<double, Azimuth>(-2, Azimuth.ANY);
            }

            if (startPoint.Longitude == endPoint.Longitude && startPoint.Latitude == endPoint.Latitude)
            {
                return new Tuple<double, Azimuth>(-1, Azimuth.NONE);
            }
            
            if (Math.Abs(startPoint.Latitude) == 90 && endPoint.Latitude == startPoint.Latitude)
            {
                return new Tuple<double, Azimuth>(-1, Azimuth.NONE);
            }

            if (Math.Abs(startPoint.Latitude) == 90 || Math.Abs(endPoint.Latitude) == 90)
            {
                return new Tuple<double, Azimuth>(180, Azimuth.CONCRETE);
            }

            if (startPoint.Longitude == endPoint.Longitude)
            {
                if (startPoint.Latitude - endPoint.Latitude > 0)
                {
                    return new Tuple<double, Azimuth>(0, Azimuth.CONCRETE);
                }
                if (startPoint.Latitude - endPoint.Latitude < 0)
                {
                    return new Tuple<double, Azimuth>(180, Azimuth.CONCRETE);
                }
            }
            
            double azimuth = RadiansToDegrees(Math.Atan2(Math.Sin(DegreesToRadians(endPoint.Longitude) - DegreesToRadians(startPoint.Longitude)) * Math.Cos(DegreesToRadians(endPoint.Latitude)),
                Math.Cos(DegreesToRadians(startPoint.Latitude)) * Math.Sin(DegreesToRadians(endPoint.Latitude)) - Math.Sin(DegreesToRadians(startPoint.Latitude)) *
                Math.Cos(DegreesToRadians(endPoint.Latitude)) * Math.Cos(DegreesToRadians(endPoint.Longitude) - DegreesToRadians(startPoint.Longitude))));
            if (azimuth < 0)
            {
                azimuth += 360;
            }

            return new Tuple<double, Azimuth>(azimuth, Azimuth.CONCRETE);
        }

        public static double GetDistance(Point startPoint, Point endPoint)
        {
            double a = Math.Pow(Math.Sin((DegreesToRadians(endPoint.Latitude) - DegreesToRadians(startPoint.Latitude)) / 2), 2) + 
                       Math.Cos(DegreesToRadians(startPoint.Latitude)) * Math.Cos(DegreesToRadians(endPoint.Latitude)) * 
                                Math.Pow(Math.Sin((DegreesToRadians(endPoint.Longitude) - DegreesToRadians(startPoint.Longitude)) / 2), 2);
            return 2 * EARTH_RADIUS * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));
        }

        private static double DegreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180;
        }

        private static double RadiansToDegrees(double radians)
        {
            return radians * 180 / Math.PI;
        }
    }
}
