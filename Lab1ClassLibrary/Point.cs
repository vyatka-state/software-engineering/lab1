﻿using System;

namespace Lab1ClassLibrary
{
    public class Point
    {
        private double longitude;
        private double latitude;

        public Point(double longitude, double latitude)
        {
            if (longitude < -180 || longitude > 180)
            {
                throw new ArgumentOutOfRangeException("Значение широты должно быть от -180 до 180");
            }
            if (latitude < -90 || latitude > 90)
            {
                throw new ArgumentOutOfRangeException("Значение долготы должно быть от -90 до 90");
            }

            this.longitude = longitude;
            this.latitude = latitude;
        }
        
        public double Longitude => longitude;

        public double Latitude => latitude;
    }
}
